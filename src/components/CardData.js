import fifa from '../assets/fifa.png'
import gta from '../assets/gta.png'
import swit from '../assets/swit.png'
import subway from '../assets/subway.png'
import templerun from '../assets/templerun.png'
import sa from '../assets/sa.png'
import cookingmama from '../assets/cookingmama.png'
import harvestmoon from '../assets/harvestmoon.png'


const CardData = [
    {
        img: fifa,
        title: 'FIFA',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/fifa'
    },
    {
        img: gta,
        title: 'GTA V',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/gtav'
    },
    {
        img: swit,
        title: 'SWIT',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/rockpaper'
    },
    {
        img: subway,
        title: 'Subway Surf',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/subwaysurf'
        
    },
    {
        img: templerun,
        title: 'Temple Run',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/templerun'
    },
    {
        img: sa,
        title: 'GTA SAN ANDREAS',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/gtasa'
    },
    {
        img: cookingmama,
        title: 'Cooking Mama',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/cookingmama'
    },
    {
        img: harvestmoon,
        title: 'Harvest Moon',
        desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris dui, tincidunt vel erat et, accumsan aliquam augue. Cras a augue a lectus accumsan efficitur et at nisl. Suspendisse faucibus dictum turpis imperdiet ornare. ',
        page: '/game/harvestmoon'
    },

];

export default CardData;