import React from "react";
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';

const Card = (props) => {
    return (
        <>
            {props.details.map((value, index) => (
                <div className="card" key={index}>
                    <div className="card-image">
                        <img src={value.img} />
                    </div>
                    <div>
                        <p className="card-title">{value.title}</p>
                        <p className="desc">{value.desc}</p>
                    </div>
                    <div className="btn">
                        
                        <Button
                            variant="contained">
                            <Link style={{ textDecoration: "none", color: "White" }} to='/game'>
                                PLAY
                            </Link>
                        </Button>
                        <Button
                            variant="contained">
                            <Link style={{ textDecoration: "none", color: "White" }} to={value.page}>
                                DETAIL
                            </Link>
                        </Button>
                    </div>
                </div>
            ))};
        </>
    );
};

export default Card
