import { BrowserRouter as Router, Routes, Route, } from 'react-router-dom';
import './App.css';
import Navigation from './components/Navigation'
import SigninNavigation from './components/SigninNavigation'
import Login from './components/Login'
import Register from './components/Register'
import LandingPage from './components/landingPage'
import GameList from './components/GameList'

import Profile from './components/Profile'
import GameDetail from './components/GameDetail'

import Home from './components/Home'
import Game from './components/game'

import Rockpaper from './components/Detail-1';
import Newfifa from './components/Detail-2';
import Newgta from './components/Detail-3';
import Newsubway from './components/Detail-4';
import Newsa from './components/Detail-5';
import Newcookingmama from './components/Detail-6';
import Newtemplerun from './components/Detail-7';
import Newharvestmoon from './components/Detail-8';






function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<><Navigation /><LandingPage /></>} />
        <Route path='/login' element={<><Navigation /><Login /></>} />
        <Route path='/register' element={<><Navigation /><Register /></>} />

        <Route path='/gameList' element={<><SigninNavigation /><GameList /></>} />
        <Route path='/Score' element={<><SigninNavigation /><Rockpaper /></>} />
        <Route path='/Profile' element={<><SigninNavigation /><Profile /></>} />
        <Route path='/gameDetail' element={<><Navigation /><GameDetail /></>} />
        <Route path='/game' element={<><SigninNavigation /><Game /></>} />
        <Route path='/Home' element={<><SigninNavigation /><Home /></>} />

        <Route path='/game/rockpaper' element={<><SigninNavigation /><Rockpaper /></>} />
        <Route path='/game/fifa' element={<><SigninNavigation /><Newfifa /></>} />
        <Route path='/game/gtav' element={<><SigninNavigation /><Newgta /></>} />
        <Route path='/game/subwaysurf' element={<><SigninNavigation /><Newsubway /></>} />

        <Route path='/game/gtasa' element={<><SigninNavigation /><Newsa /></>} />
        <Route path='/game/cookingmama' element={<><SigninNavigation /><Newcookingmama /></>} />
        <Route path='/game/templerun' element={<><SigninNavigation /><Newtemplerun /></>} />
        <Route path='/game/harvestmoon' element={<><SigninNavigation /><Newharvestmoon /></>} />


      </Routes>
    </Router>
  );
}

export default App;
